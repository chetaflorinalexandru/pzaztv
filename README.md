# pzaztv

## Previously to install

[Docker](https://www.docker.com/products/docker-desktop/) + [Node](https://nodejs.org/en/download).

## Project Setup

Go to frontend directory and run
```sh
docker compose up -d
```

Go to backend directory and run
```sh
docker compose up -d
```

Run this command witch will get you into a docker container
```sh
docker exec -it backend-web-1 bash
```

Into container you have to run these commands.
```sh
python manange.py migrate && python manage.py createsuperuser
```

## Last step
You can open this url http://localhost:5173/ and login.

