Django>=3.0,<4.0
psycopg2>=2.8
djangorestframework>=3
djangorestframework-simplejwt>=5
django-rest-marshmallow>=3
marshmallow>=3.19
django-cors-headers>=3.13.0
django-environ>=0.11.2