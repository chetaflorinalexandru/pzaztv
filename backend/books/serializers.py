from rest_framework import serializers
from .models import Books

class BooksSerializer(serializers.ModelSerializer):

    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(max_length=1500,required=True)
    marketplace_source = serializers.CharField(max_length=1500,required=True)
    user = serializers.CharField(max_length=1500,read_only=True)
    created_at = serializers.DateTimeField(format="%m-%d-%Y %I:%M %p",required=False)
    updated_at = serializers.DateTimeField(format="%m-%d-%Y %I:%M %p",required=False)

    class Meta:
        model = Books
        fields = '__all__'

    def create(self, validated_data,instance):
        return instance.objects.create(**validated_data)

    def update(self, validated_data,instance):
        for key, value in validated_data.items():
            setattr(instance, key, value)
        instance.save()
        return instance