from django.urls import path

from .views import BooksView

urlpatterns = [
    path('', BooksView.as_view(), {'id': None}),
    path('<int:id>', BooksView.as_view()),
]