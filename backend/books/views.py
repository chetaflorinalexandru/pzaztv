from library.response import MakeResponse
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from .models import Books
from .serializers import BooksSerializer


class BooksView(APIView):

    permission_classes = [IsAuthenticated]
    queryset = Books

    def get(self, request,id):
        if id:
            serializer_data = BooksSerializer(self.queryset.objects.filter(user=request.user).get(pk=id))
        else:
            serializer_data = BooksSerializer(self.queryset.objects.filter(user=request.user).order_by(
                '-created_at').all(), many=True)
        return MakeResponse(data=serializer_data.data).response()

    def put(self,request,id):
        serializer_data = BooksSerializer(data=request.data)
        if serializer_data.is_valid():
            book = self.queryset.objects.filter(user=request.user).get(pk=id)
            if book:
                serializer_data.update(validated_data= {**serializer_data.validated_data,**{'user':request.user}},instance=book)
                return MakeResponse(data=serializer_data.validated_data).response()
        return MakeResponse(data=serializer_data.errors,status=404).response()

    def post(self,request,id):
        serializer_data = BooksSerializer(data=request.data)
        if serializer_data.is_valid():
            serializer_data.create(validated_data= {**serializer_data.validated_data,**{'user':request.user}},instance=Books)
            return MakeResponse(data=serializer_data.validated_data).response()
        return MakeResponse(data=serializer_data.errors,status=404).response()

    def delete(self,request,id):
        book = self.queryset.objects.filter(user=request.user).get(pk=id)
        if book:
            book.delete()
            return MakeResponse(data=[]).response()
        return MakeResponse(data=[],status=404).response()

