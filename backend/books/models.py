from django.db import models
from django.contrib import admin
from account.models import User

class Books(models.Model):

    name = models.CharField(max_length=1500,null=True)
    marketplace_source = models.CharField(max_length=1500,null=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
    class Meta:
        verbose_name = 'Book'
        verbose_name_plural = 'Books'

class BooksAdmin(admin.ModelAdmin):
    list_display = ["id","name","marketplace_source"]

admin.site.register(Books,BooksAdmin)