from django.contrib import admin
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):

    def __str__(self):
        return self.username
    class Meta:
        verbose_name_plural = 'Users'

class UserAdmin(admin.ModelAdmin):
    list_display= ('id','username', 'first_name', 'last_name')

admin.site.register(User,UserAdmin)

