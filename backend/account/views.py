from rest_framework.views import APIView
from account.schemas import UserSchema
from library.response import MakeResponse

from rest_framework.permissions import IsAuthenticated,AllowAny

class ProfileView(APIView):

    permission_classes = [IsAuthenticated]

    def get(self, request):
        return MakeResponse(data=UserSchema().dump(request.user)).response()

    def put(self,request):
        user = UserSchema(data=request.data,instance=request.user)
        if user.is_valid():
            user.update(validated_data=request.data,instance=request.user)
            return MakeResponse(data=user.data).response()
        return MakeResponse(data=user.errors,status=404).response()
