from django.urls import path

from account.views import ProfileView

urlpatterns = [
    path('user/profile/', ProfileView.as_view(),name='profile'),
]