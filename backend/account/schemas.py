from rest_marshmallow import Schema, fields
from .models import User

class UserSchema(Schema):
    id = fields.Integer(required=True,dump_only=True)
    first_name = fields.String()
    last_name = fields.String()
    email = fields.Email(required=True,unqiue=True)
    username = fields.String(required=True,unqiue=True)

    class Meta:
        model = User


