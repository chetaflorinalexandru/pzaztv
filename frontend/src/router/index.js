import { createWebHistory, createRouter } from "vue-router";
import Login from "@/views/Login.vue";
import Books from "@/views/Books.vue";
import EditBook from "@/views/EditBook.vue";
import CreateBook from "@/views/CreateBook.vue";

const routes = [
    {
        path: "/",
        name: "Login",
        component: Login,
    },
    {
        path: "/books",
        name: "Books",
        component: Books,
    },
    {
        path: "/books/edit/:id",
        name: "Edit book",
        component: EditBook,
        props: route => ({
            id: route.params.id
        })
    },
    {
        path: "/books/add",
        name: "Add book",
        component: CreateBook,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;